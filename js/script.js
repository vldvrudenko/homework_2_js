"use strict";

let number;

do {
    number = prompt("Give me a number");
} while (number === null || number === "" || isNaN(+number) || Number.isInteger(+number) === false);

if (number > 0) {
    for (let i = 0; i <= number; i++) {
        if ( i % 5 === 0) {
            console.log(i) 
        }  
    }  
} else console.log("Sorry! No numbers");


